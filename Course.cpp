/* version 1.0 of my school*/
#include "Course.h"
#include "Student.h"
#include <iostream>
#include <string>

#define MAX_STUDENTS 10

#define MENU_STUDENT_DETAILS 1
#define MENU_AVERAGE 2
#define MENU_PRINT_COURSES 3
#define MENU_EXIT 4

using namespace std;

void Course::init(string name, int test1, int test2, int exam)
{
	_name = name;
	_grades[0] = test1;
	_grades[1] = test2;
	_grades[2] = exam;
}

int* Course::getGradesList()
{
	return _grades;
}
string Course::getName()
{
	return _name;
}
double Course::getFinalGrade()
{
	return (_grades[0] * 0.25 + _grades[1] * 0.25 + _grades[2] * 0.5);
}
