/* version 1.0 of my school*/
#include "Course.h"
#include "Student.h"
#include <iostream>
#include <string>

#define MAX_STUDENTS 10

#define MENU_STUDENT_DETAILS 1
#define MENU_AVERAGE 2
#define MENU_PRINT_COURSES 3
#define MENU_EXIT 4

using namespace std;

void Student::init(string name, Course** courses, int crsCount)
{
	_name = name;
	_Courses = courses;
	_crsCount = crsCount;



}
string Student::getName()
{
	return _name;
}
void Student::setName(string name)
{
	_name = name;
}

double Student::getAvg()
{
	double sum = 0;
	for (int i = 0; i < _crsCount; i++)
	{
		sum += _Courses[i]->getFinalGrade();

	}

	return sum / _crsCount;
}

int Student::getCrsCount()
{
	return _crsCount;
}
Course** Student::getCourses()
{
	return _Courses;
}